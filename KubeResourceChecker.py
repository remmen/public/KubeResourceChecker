import argparse
import sys
from kubernetes import client, config
from prettytable import PrettyTable
from colorama import init, Fore

def fetch_resources(api_instance, namespace, resource_type, filter=None, exclude=None):
    resources = {}
    if namespace:
        resource_list = api_instance.list_namespaced_secret(namespace) if resource_type == 'secret' else api_instance.list_namespaced_config_map(namespace)
    else:
        resource_list = api_instance.list_secret_for_all_namespaces() if resource_type == 'secret' else api_instance.list_config_map_for_all_namespaces()
       
    idx = 0
    for item in resource_list.items:
        if (filter is None or any(f in item.metadata.name for f in filter)) and (exclude is None or not any(e in item.metadata.name for e in exclude)):
            resources[idx] = {'name': item.metadata.name, 'namespace': item.metadata.namespace,'resource': item, 'used': False, 'locations': []}
            idx += 1

    return resources

def fetch_pods(api_instance, namespace):
    if namespace:
        pod_list = api_instance.list_namespaced_pod(namespace)
    else:
        pod_list = api_instance.list_pod_for_all_namespaces()
    pods = []
    for item in pod_list.items:
        pods.append(item)
    return pods

def fetch_deployments(api_instance_apps, namespace):
    if namespace:
        deployment_list = api_instance_apps.list_namespaced_deployment(namespace)
    else:
        deployment_list = api_instance_apps.list_deployment_for_all_namespaces()
    deployments = []
    for item in deployment_list.items:
        deployments.append(item)
    return deployments

def fetch_ingresses(api_instance, namespace):
    if namespace:
        ingress_list = api_instance.list_namespaced_ingress(namespace)
    else:
        ingress_list = api_instance.list_ingress_for_all_namespaces()
    ingresses = []
    for item in ingress_list.items:
        ingresses.append(item)
    return ingresses

def check_resource_usage(resources, pods, resource_type):
    for pod in pods:
        for container in pod.spec.containers:
            if container.env_from:
                for env_from_source in container.env_from:
                    if resource_type == 'configmap' and env_from_source.config_map_ref:
                        resource_name = env_from_source.config_map_ref.name
                        for idx in resources:
                            if resources[idx]['name'] == resource_name:
                                resources[idx]['used'] = True
                                resources[idx]['locations'].append(
                                    f"Pod: {pod.metadata.namespace}/{pod.metadata.name} (envFrom)"
                                )
                    elif resource_type == 'secret' and env_from_source.secret_ref:
                        resource_name = env_from_source.secret_ref.name
                        for idx in resources:
                            if resources[idx]['name'] == resource_name:
                                resources[idx]['used'] = True
                                resources[idx]['locations'].append(
                                    f"Pod: {pod.metadata.namespace}/{pod.metadata.name} (envFrom)"
                                )
            if container.env:
                for env_var in container.env:
                    if env_var.value_from and env_var.value_from.config_map_key_ref and resource_type == 'configmap':
                        resource_name = env_var.value_from.config_map_key_ref.name
                        for idx in resources:
                            if resources[idx]['name'] == resource_name:
                                resources[idx]['used'] = True
                                resources[idx]['locations'].append(
                                    f"Pod: {pod.metadata.namespace}/{pod.metadata.name} (env)"
                                )
                    elif env_var.value_from and env_var.value_from.secret_key_ref and resource_type == 'secret':
                        resource_name = env_var.value_from.secret_key_ref.name
                        for idx in resources:
                            if resources[idx]['name'] == resource_name:
                                resources[idx]['used'] = True
                                resources[idx]['locations'].append(
                                    f"Pod: {pod.metadata.namespace}/{pod.metadata.name} (env)"
                                )
            if container.volume_mounts:
                for volume_mount in container.volume_mounts:
                    if volume_mount.name in pod.spec.volumes:
                        volume = next((vol for vol in pod.spec.volumes if vol.name == volume_mount.name), None)
                        if volume and volume.secret and resource_type == 'secret':
                            resource_name = volume.secret.secret_name
                            for idx in resources:
                                if resources[idx]['name'] == resource_name:
                                    resources[idx]['used'] = True
                                    resources[idx]['locations'].append(
                                        f"Pod: {pod.metadata.namespace}/{pod.metadata.name} (volume)"
                                    )
                        elif volume and volume.config_map and resource_type == 'configmap':
                            resource_name = volume.config_map.name
                            for idx in resources:
                                if resources[idx]['name'] == resource_name:
                                    resources[idx]['used'] = True
                                    resources[idx]['locations'].append(
                                        f"Pod: {pod.metadata.namespace}/{pod.metadata.name} (volume)"
                                    )

                                    resources[idx]['locations'].append(f'deployment: {deployment.metadata.name}')
def check_resource_usage_in_deployments(resources, deployments, resource_type):
    for deployment in deployments:
        for idx in resources:
            if deployment.spec.template.spec.volumes:
                for volume in deployment.spec.template.spec.volumes:
                    if volume.secret and volume.secret.secret_name == resources[idx]['name'] and resource_type == 'secret':
                        resources[idx]['used'] = True
                        resources[idx]['locations'].append(f'deployment: {deployment.metadata.name}')
                    elif volume.config_map and volume.config_map.name == resources[idx]['name'] and resource_type == 'configmap':
                        resources[idx]['used'] = True
                        resources[idx]['locations'].append(f'deployment: {deployment.metadata.name}')
            if deployment.spec.template.spec.containers:
                for container in deployment.spec.template.spec.containers:
                    if container.env:
                        for env in container.env:
                            if env.value_from and env.value_from.config_map_key_ref and env.value_from.config_map_key_ref.name == resources[idx]['name'] and resource_type == 'configmap':
                                if f'deployment: {deployment.metadata.name}' not in resources[idx]['locations']:
                                    resources[idx]['locations'].append(f'deployment: {deployment.metadata.name}')
                            elif env.value_from and env.value_from.secret_key_ref and env.value_from.secret_key_ref.name == resources[idx]['name'] and resource_type == 'secret':
                                if f'deployment: {deployment.metadata.name}' not in resources[idx]['locations']:
                                    resources[idx]['locations'].append(f'deployment: {deployment.metadata.name}')

def check_resource_usage_in_ingresses(resources, ingresses, resource_type):
    if resource_type != 'secret':  # Skip if the resource is not a secret
        return
    for ingress in ingresses:
        if ingress.spec.tls:
            for tls in ingress.spec.tls:
                for idx in resources:
                    if tls.secret_name == resources[idx]['name']:
                        resources[idx]['used'] = True
                        resources[idx]['locations'].append(f"Ingress: {ingress.metadata.name}")


def label_unused_resources(api_instance, resources, label, resource_type):
    for idx in resources:
        if not resources[idx]['used']:
            resource = resources[idx]['resource']
            if resource.metadata.labels is None:
                resource.metadata.labels = {}
            resource.metadata.labels.update(label)
            if resource_type == 'configmap':
                api_instance.patch_namespaced_config_map(resources[idx]['name'], resource.metadata.namespace, resource)
            else:
                api_instance.patch_namespaced_secret(resources[idx]['name'], resource.metadata.namespace, resource)

def delete_unused_resources(api_instance, resources, resource_type):
    print(f"WARNING: You are about to delete unused {resource_type}. This operation is irreversible.")
    confirm = input("Do you want to proceed? (y/n): ")
    if confirm.lower() not in ('y', 'yes'):
        print("Operation cancelled.")
        sys.exit(0)

    for idx in resources:
        if not resources[idx]['used']:
            if resource_type == 'configmap':
                api_instance.delete_namespaced_config_map(
                    name=resources[idx]['name'],
                    namespace=resources[idx]['resource'].metadata.namespace,
                    body=client.V1DeleteOptions()
                )
            else:
                api_instance.delete_namespaced_secret(
                    name=resources[idx]['name'],
                    namespace=resources[idx]['resource'].metadata.namespace,
                    body=client.V1DeleteOptions()
                )

def main(namespace, label, delete, filter, exclude, resource_type):
    init(autoreset=True)
    config.load_kube_config()
    api_instance_core = client.CoreV1Api()
    api_instance_apps = client.AppsV1Api()
    api_instance_networking = client.NetworkingV1Api()

    resources = {}
    pods = []
    deployments = []
    ingresses = []
   
    resources = fetch_resources(api_instance_core, namespace, resource_type, filter, exclude)
    pods = fetch_pods(api_instance_core, namespace)
    deployments = fetch_deployments(api_instance_apps, namespace)
    ingresses = fetch_ingresses(api_instance_networking, namespace)

    check_resource_usage(resources, pods, resource_type)
    check_resource_usage_in_deployments(resources, deployments, resource_type)
    check_resource_usage_in_ingresses(resources, ingresses, resource_type)

    if label is not None:
        label_unused_resources(api_instance_core, resources,  {label.split('=')[0]: label.split('=')[1]} , resource_type)

    if delete:
        delete_unused_resources(api_instance_core, resources, resource_type)

    if namespace is None:
        table = PrettyTable()
        table.field_names = ["Namespace", f"Total {resource_type.capitalize()}", f"Unused {resource_type.capitalize()}"]

        allnamespaces = api_instance_core.list_namespace().items
        for ns in allnamespaces:
            if (filter is None or any(f in ins.metadata.name for f in filter)) and (exclude is None or not any(e in ns.metadata.name for e in exclude)):
                total_resources = 0
                unused_resources = 0
                for idx in resources:
                    if resources[idx]['namespace'] == ns.metadata.name and not resources[idx]['used']: unused_resources +=1
                    if resources[idx]['namespace'] == ns.metadata.name: total_resources +=1
                table.add_row([ns.metadata.name, total_resources, unused_resources])
        print(table)

    else:
        table = PrettyTable()
        table.field_names = ["Name", "Used", "Locations"]
        unused_resources = 0
        for idx in resources:
            table.add_row([resources[idx]['name'], Fore.GREEN + "Yes" if resources[idx]['used'] else Fore.RED + "No", "\n".join(resources[idx]['locations'])])
            if not resources[idx]['used']: unused_resources +=1
        print(table)
        total_resources = len(resources)
        print("In total", unused_resources,"are not used out of" ,total_resources )
           

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Check unused configmaps or secrets.')
    parser.add_argument('-n', '--namespace', help='Kubernetes namespace')
    parser.add_argument("-l", "--label", help="Label to add to unused ConfigMaps or Secrets in 'key=value' format.")
    parser.add_argument('-d', '--delete', action='store_true', help='Delete unused configmaps or secrets')
    parser.add_argument('-f', '--filter', nargs='+', help='List of keywords to filter configmaps or secrets')
    parser.add_argument('-e', '--exclude', nargs='+', help='List of keywords to exclude configmaps or secrets')
    parser.add_argument('-r', '--resource', choices=['configmap', 'secret'], default='configmap', help='Resource type to check (default: configmap)')
    args = parser.parse_args()
    main(args.namespace, args.label, args.delete, args.filter, args.exclude, args.resource)
