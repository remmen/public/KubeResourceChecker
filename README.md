# KubeResourceChecker

The KubeResourceChecker is a Python script that checks the usage of Kubernetes ConfigMaps and Secrets across all namespaces or a specified namespace. It indicates if a ConfigMap or Secret is used or not, and provides its location if it is in use.

**Note:** This script was mostly written with ChatGPT


## Installation

1. Ensure you have Python 3.6 or later installed.
2. Install the necessary Python packages:

    ```bash
    pip install kubernetes prettytable colorama argparse
    ```

## Usage

You can run the script in your terminal/command line. The script requires the kubeconfig file to interact with the cluster.

```bash
python KubeResourceChecker.py [-n NAMESPACE] [-l LABEL] [-d] [-f FILTER] [-e EXCLUDE] [-r RESOURCE]
```

- -n or --namespace: Specify the namespace to fetch ConfigMaps and Secrets from.
- -l or --label: Specify a label to tag unused ConfigMaps and Secrets.
- -d: Delete unused ConfigMaps and Secrets.
- -f or --filter: Filter ConfigMaps and Secrets by a specific string or comma-separated list of strings.
- -e or --exclude: Exclude ConfigMaps and Secrets by a specific string or comma-separated list of strings.
- -r or --resource: Specify the resource type to check (either configmaps or secrets).

**Note** If no namespace is submitted, it will list an overall statistic 


## Example

### Check usage of ConfigMaps in a specific namespace

```bash
    python KubeResourceChecker.py -n mynamespace -r configmaps
```

### Check usage of Secrets in a specific namespace

```bash
    python KubeResourceChecker.py -n mynamespace -r secrets
```

### Label all unused ConfigMaps with a specific label

```bash
    python KubeResourceChecker.py -l unused=true -r configmaps
```

### Delete all unused ConfigMaps

```bash
    python KubeResourceChecker.py -d -r configmaps
```
**NOTE:** Deleting unused ConfigMaps or Secrets is irreversible. If you are uncertain about deleting a ConfigMap or Secret, you can use the `-l` or `--label` option to label the resource first, then confirm its usage before deleting it.


### Filter ConfigMaps by a specific string

```bash
    python KubeResourceChecker.py -f mystring -r configmaps
```

### Exclude ConfigMaps containing a specific string

```bash
    python KubeResourceChecker.py -e mystring -r configmaps
```